# mnist多种分类方法实现 


## LeNet-5
## ViT 
## DeiT (distilled by LeNet-5)

# 实验结果
| 模型 | 测试集准确率 |  
| :-----| :----: |
| LeNet-5 | 99.11% |
| ViT | 97.99% |
| DeiT | 97.95% |
| ViT-distill | 98.03% |


